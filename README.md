# ifml-io modeler deployment

## Building

You need a [NodeJS](http://nodejs.org) development stack with [yarn](https://yarnpkg.com/) installed to build the project.

To install all dependencies execute

```
yarn install
```

Build the application via

```
yarn all
```

You may also spawn a development setup by executing

```
yarn dev
```

for productive build execute (deployed into `dist` folder):

```
yarn build
```

for docker deployment execute:

```
docker-compose up
```
