const CopyWebpackPlugin = require('copy-webpack-plugin');

var dirServer = __dirname + '/dist';
var dirClient = dirServer + '/public';

module.exports = {
  entry: {
    bundle: ['./app/app.js']
  },
  output: {
    path: dirClient,
    filename: 'app.js'
  },
  module: {
    rules: [{
      test: /\.ifml/,
      use: 'raw-loader'
    }]
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [{
          from: 'assets/**',
          to: dirClient + '/vendor/ifml-js',
          context: '../ifml-js/dist/'
        },
        {
          from: '**/*.{html,css}',
          to: dirClient,
          context: 'app/'
        },
        {
          from: '**/*',
          to: dirServer,
          context: 'deploy/'
        },
      ]
    })
  ],
  mode: 'development',
  devtool: 'source-map',
};