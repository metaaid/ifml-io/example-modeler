FROM node:alpine as run

WORKDIR /app

COPY dist/* /app/
COPY deploy/* /app/

RUN apk add --update nodejs yarn && \
    yarn install

ENTRYPOINT [ "yarn", "start" ]
